TEMPLATE_ROOT_DIR ?= ~/templates
STOW := $(shell command -v stow 2> /dev/null)

default:
	@echo "Please specify 'link' or 'unlink' target."

check_stow:
ifndef STOW
	$(error "GNU stow is not installed. Please install it before using this Makefile.")
endif

link: check_stow
	@if [ ! -d $(TEMPLATE_ROOT_DIR) ]; then \
		mkdir -p $(TEMPLATE_ROOT_DIR); \
		echo "Template directory '$(TEMPLATE_ROOT_DIR)' created."; \
	fi
	@$(STOW) -v -t $(TEMPLATE_ROOT_DIR) --ignore='Makefile' --restow */

unlink: check_stow
	@if [ -d $(TEMPLATE_ROOT_DIR) ]; then \
		$(STOW) -v -t $(TEMPLATE_ROOT_DIR) --delete */; \
		if [ -z "$$(ls -A $(TEMPLATE_ROOT_DIR))" ]; then \
			rmdir $(TEMPLATE_ROOT_DIR); \
			echo "Template directory '$(TEMPLATE_ROOT_DIR)' deleted."; \
		fi; \
	else \
		echo "Template directory '$(TEMPLATE_ROOT_DIR)' does not exist."; \
	fi
