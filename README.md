# Templates

This repository contains project templates that I commonly use.

Each template corresponds to the content of a certain folder. Templates are
categorised and located in a directory named with the category within the
[`templates`](./templates/) directory.


## List of available templates

### Software development

- [`bash-advanced`](./templates/software_development/bash-advanced/),
  bash project with subcommands and integration tests
  (based on [*shell-ninja* repository](https://github.com/ro14nd-talks/shell-ninja))


## Usage

Locally, I aggregate templates from multiple sources in a certain template
directory. This repository includes a Makefile to manage the creation and removal
of symbolic links of its templates to such a template directory.

### Environment Variables

#### `TEMPLATE_ROOT_DIR`

The Makefile uses the environment variable, `TEMPLATE_ROOT_DIR`, to determine
the directory where templates should be linked to. You can set this variable
to customise the location. If `TEMPLATE_ROOT_DIR` is not set, the default value
`~/templates` will be used.

#### Example:

```bash
export TEMPLATE_ROOT_DIR=/path/to/custom/directory
```

### Makefile Commands

- **`make link`**: Creates symbolic links for templates. If the specified root
  directory (`TEMPLATE_ROOT_DIR`) does not exist, it will be created.

- **`make unlink`**: Removes symbolic links for templates. If the template
  root directory is empty after unlinking, it will be deleted.

### Notes

- The Makefile uses [GNU Stow](https://www.gnu.org/software/stow/) to manage
  symbolic links, ensure that it is installed on your system.

- The Makefile assumes a Unix-like environment, adjust it as needed for other
  operating systems.


## License

The content of this repository is provided under the [MIT License](LICENSE).
