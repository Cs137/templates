# Contributing to YourProjectName

Thank you for considering contributing to YourProjectName! Whether it's reporting bugs, suggesting features, or submitting pull requests, your contributions are welcome.

## How to Contribute

1. Fork the project repository on [Codeberg](https://codeberg.org/YourUserName/YourProjectName).
2. Clone your forked repository locally: `git clone https://codeberg.org/yourusername/[Your Project Name].git`
3. Create a new branch for your feature or bug fix: `git checkout -b feature-name`
4. Make your changes and ensure tests pass (if applicable).
5. Commit your changes with a descriptive commit message: `git commit -m "Add your message here"`
6. Push your branch to your forked repository: `git push origin feature-name`
7. Open a merge request on [Codeberg](https://codeberg.org/YourUserName/YourProjectName) from your branch to the main repository's `main` branch.

## Code Style

Follow the established coding style in the project. If there's no specific style guide, maintain consistency with the existing code.

## Reporting Issues

If you encounter issues, please use the [Codeberg issue tracker](https://codeberg.org/YourUserName/YourProjectName/issues) to report them. Provide as much detail as possible, including the version of the project and steps to reproduce the issue.

## Feature Requests

If you have a feature request, please open an issue on [Codeberg](https://codeberg.org/YourUserName/YourProjectName/issues) with a detailed description. Discuss the proposed feature with maintainers and the community before starting development.

### Testing

If you intend to implement a feature, include tests for the feature as well.
Integration tests are performed with [bats](https://github.com/bats-core/bats-core).
Ensure bats is installed on your machine and run `bats tests` to starting the tests.

## License

By contributing to this project, you agree that your contributions will be licensed under the project's [LICENSE](LICENSE).

Thank you for contributing!
