#!/usr/bin/env bats

load test_helper

@test "No mandatory option given" {
  run $BATS_TEST_DIRNAME/../YourProjectName example
  [ $status -eq 1 ]
  assert_regexp "No test"
  assert_regexp "--test"
  assert_regexp "example"
}
